package golife

import (
	"fmt"
	"slices"
)

// Pair is a pair of integers that represent a cell's position
type Pair struct {
	X int
	Y int
}

// willLive returns true if a cell will live in the next generation
// given the current state of the cell and the number of live neighbors
// it has.
func willLive(isAlive bool, numOfLiveNeighbors int) bool {
	if isAlive && numOfLiveNeighbors == 2 {
		return true
	}
	if numOfLiveNeighbors == 3 {
		return true
	}
	return false
}

// generateSignals returns the surrounding signals that a cell at a given position
func generateSignals(position Pair) []Pair {
	return []Pair{
		{position.X - 1, position.Y - 1}, {position.X, position.Y - 1}, {position.X + 1, position.Y - 1},
		{position.X - 1, position.Y}, {position.X + 1, position.Y},
		{position.X - 1, position.Y + 1}, {position.X, position.Y + 1}, {position.X + 1, position.Y + 1},
	}
}

// getAllSignals returns all signals from each of the live cells
// in the current generation and returns them as a slice of pairs.
func getAllSignals(liveCells []Pair) []Pair {
	signals := []Pair{}
	for _, cell := range liveCells {
		signals = append(signals, generateSignals(cell)...)
	}
	return signals
}

// getSignalCountMap returns a map of signals to the number of occurances
// of that signal in the slice of signals.
func getSignalCountMap(signals []Pair) map[Pair]int {
	counts := make(map[Pair]int)
	for _, signal := range signals {
		counts[signal] = countOccurances(signals, signal)
	}
	return counts
}
func countOccurances(signals []Pair, signal Pair) int {
	count := 0
	for _, s := range signals {
		if s == signal {
			count += 1
		}
	}
	return count
}

// GetNextGeneration returns the next generation of live cells
// given the current generation of live cells.
func GetNextGeneration(liveCells []Pair) []Pair {
	signals := getAllSignals(liveCells)
	counts := getSignalCountMap(signals)
	nextGeneration := []Pair{}
	for signal, count := range counts {
		isAlive := slices.Contains(liveCells, signal)
		goingToLive := willLive(isAlive, count)
		if goingToLive {
			nextGeneration = append(nextGeneration, signal)
		}
	}
	return nextGeneration
}

// RunTerminalGameOfLife runs the game of life in the terminal
func RunTerminalGameOfLife(liveCells []Pair) []Pair {
	// clear the terminal
	fmt.Print("\033[H\033[2J")

	xValues := []int{}
	yValues := []int{}
	for _, cell := range liveCells {
		xValues = append(xValues, cell.X)
		yValues = append(yValues, cell.Y)
	}

	xMinValue := slices.Min(xValues)
	xMaxValue := slices.Max(xValues)
	yMinValue := slices.Min(yValues)
	yMaxValue := slices.Max(yValues)

	xIterRange := []int{}
	yIterRange := []int{}

	for i := xMinValue - 6; i <= xMaxValue+6; i++ {
		xIterRange = append(xIterRange, i)
	}
	for i := yMinValue - 6; i <= yMaxValue+6; i++ {
		yIterRange = append(yIterRange, i)
	}

	matrixString := ""

	for _, y := range yIterRange {
		for _, x := range xIterRange {
			if slices.Contains(liveCells, Pair{x, y}) {
				matrixString += "X"
			} else {
				matrixString += " "
			}
		}
		matrixString += "\n"
	}
	fmt.Println(matrixString)
	return GetNextGeneration(liveCells)
}

// func main() {
// 	liveCells := []pair{
// 		{0, 0}, {1, 0}, {1, -1}, {2, -1}, {2, -2}, {3, -2},
// 	}
// 	for i := 0; i < 100; i++ {
// 		liveCells = RunTerminalGameOfLife(liveCells)
// 		time.Sleep(time.Second / 6)
// 	}
// }
