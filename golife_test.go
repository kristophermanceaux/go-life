package golife

import (
	"slices"
	"testing"
)

func TestWillLive(t *testing.T) {
	if willLive(true, 2) != true {
		t.Error("Expected true, got ", willLive(true, 2))
	}
	if willLive(true, 3) != true {
		t.Error("Expected true, got ", willLive(true, 3))
	}
	if willLive(true, 4) != false {
		t.Error("Expected false, got ", willLive(true, 4))
	}
	if willLive(false, 3) != true {
		t.Error("Expected true, got ", willLive(false, 3))
	}
	if willLive(false, 4) != false {
		t.Error("Expected false, got ", willLive(false, 4))
	}
}

func TestGenerateSignals(t *testing.T) {
	signals := generateSignals(Pair{0, 0})
	expected := []Pair{
		{-1, -1}, {0, -1}, {1, -1},
		{-1, 0}, {1, 0},
		{-1, 1}, {0, 1}, {1, 1},
	}
	if slices.CompareFunc(signals, expected, func(a, b Pair) int {
		if a.X != b.X || a.Y != b.Y {
			return 1
		}
		return 0
	}) != 0 {
		t.Error("Expected ", expected, ", got ", signals)
	}
}

func TestGetAllSignals(t *testing.T) {
	liveCells := []Pair{
		{0, 0}, {1, 0}, {2, 0},
	}
	signals := getAllSignals(liveCells)
	expected := []Pair{
		{-1, -1}, {0, -1}, {1, -1}, {2, -1}, {3, -1},
		{-1, 0}, {0, 0}, {1, 0}, {2, 0}, {3, 0},
		{-1, 1}, {0, 1}, {1, 1}, {2, 1}, {3, 1},
	}
	for _, signal := range signals {
		if !slices.Contains(expected, signal) {
			t.Error("Expected ", expected, ", got ", signals)
		}
	}
}

func TestGetSignalCountMap(t *testing.T) {
	signals := []Pair{
		{-1, -1}, {-1, -1}, {0, -1}, {1, -1}, {2, -1}, {3, -1},
		{-1, 0}, {0, 0}, {1, 0}, {2, 0}, {3, 0}, {3, 0},
		{-1, 1}, {0, 1}, {1, 1}, {2, 1}, {3, 1}, {3, 1},
	}
	counts := getSignalCountMap(signals)
	expected := map[Pair]int{
		{-1, -1}: 2, {0, -1}: 1, {1, -1}: 1, {2, -1}: 1, {3, -1}: 1,
		{-1, 0}: 1, {0, 0}: 1, {1, 0}: 1, {2, 0}: 1, {3, 0}: 2,
		{-1, 1}: 1, {0, 1}: 1, {1, 1}: 1, {2, 1}: 1, {3, 1}: 2,
	}
	for signal, count := range counts {
		if expected[signal] != count {
			t.Error("Expected ", expected, ", got ", counts)
		}
	}
}

func TestCountOccurances(t *testing.T) {
	signals := []Pair{
		{-1, -1}, {-1, -1}, {0, -1}, {1, -1}, {2, -1}, {3, -1},
		{-1, 0}, {0, 0}, {1, 0}, {2, 0}, {3, 0}, {3, 0},
		{-1, 1}, {0, 1}, {1, 1}, {2, 1}, {3, 1}, {3, 1},
		{0, 0}, {0, 0}, {0, 0},
	}
	if countOccurances(signals, Pair{-1, -1}) != 2 {
		t.Error("Expected 2, got ", countOccurances(signals, Pair{-1, -1}))
	}
	if countOccurances(signals, Pair{3, 0}) != 2 {
		t.Error("Expected 2, got ", countOccurances(signals, Pair{3, 0}))
	}
	if countOccurances(signals, Pair{0, 0}) != 4 {
		t.Error("Expected 4, got ", countOccurances(signals, Pair{0, 0}))
	}
}

func TestGetNextGeneration(t *testing.T) {
	liveCells := []Pair{
		{0, 0}, {1, 0}, {2, 0},
	}
	nextGeneration := GetNextGeneration(liveCells)
	expected := []Pair{
		{1, -1}, {1, 0}, {1, 1},
	}
	for _, cell := range nextGeneration {
		if !slices.Contains(expected, cell) {
			t.Error("Expected ", expected, ", got ", nextGeneration)
		}
	}
}

func TestGetNextGeneration2(t *testing.T) {
	liveCells := []Pair{
		{0, -1}, {0, 0}, {0, 1},
	}
	nextGeneration := GetNextGeneration(liveCells)
	expected := []Pair{
		{-1, 0}, {0, 0}, {1, 0},
	}
	for _, cell := range nextGeneration {
		if !slices.Contains(expected, cell) {
			t.Error("Expected ", expected, ", got ", nextGeneration)
		}
	}
}
